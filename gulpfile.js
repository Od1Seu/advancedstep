const {src, dest, watch, series } = require('gulp');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
const clean = require('gulp-clean');
const autoprefixer = require('gulp-autoprefixer');
const minifyjs = require('gulp-js-minify');
const rename = require("gulp-rename");
const imagemin = require('gulp-imagemin');
const fileinclude = require('gulp-file-include');

const path = {
    src:{
        scss: "./src/scss",
        js: "./src/js",
        img: "./src/img",
        html: "./src/html"
    },
    dist: {
        css: './dist/css',
        js: "./dist/",
        img: "./dist/"
    }
};

const dev = function () {
    browserSync.init({
        server: {
            baseDir: path.dist
        },
        port: 8080
    });
    watch(path.src.html + '**/*.html', function (cb) {
        includeHTML();
        browserSync.reload();
        cb();
    });
    watch(path.src.scss + '**/*.scss', function (cb) {
        build();
        browserSync.reload();
        cb();
    });
    watch(path.src.js + "**/*.js", function (cb) {
        minify_JS();
        browserSync.reload();
        cb();
    });
};
const image = function () {
    return src(path.src.img + "**/*.png")
        .pipe(imagemin())
        .pipe(dest(path.dist.img))
};
const minify_JS = function () {
    return src(path.src.js +'**/*.js')
        .pipe(minifyjs())
        .pipe(rename("/js/scripts.min.js"))
        .pipe(dest(path.dist.js))
};
const cleanFolder = function () {
    return src(['./dist/*', '!dist'],{ read: false })
        .pipe(clean({force: true}));
};
const includeHTML = function () {
    return src(["index.html"])
        .pipe(fileinclude())
        .pipe(dest('./dist'));
};
const reset = function () {
    return src("src/reset.css")
        .pipe(dest(path.dist.css))
};
const scssGo = function() {
    return src(path.src.scss + "/**/*.scss")
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(rename("/styles.min.css"))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(dest(path.dist.css));
};

exports.dev = dev;
exports.build = series(cleanFolder,minify_JS,image,includeHTML,scssGo,reset);
exports.cleanFolder = cleanFolder;