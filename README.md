# Project name Advanced_Step_Od1Seu!

Git - https://gitlab.com/Od1Seu/advancedstep/-/tree/master;

Dmytro Sobchenko (dimasobchenko.94@gamil.com);

## Overview
This is an adaptive site for general use.(Mobile,Table,Desktop);

## Licenses
General - MIT;

## Instructions for installing dependencies
    Open project in root folder, then open Terminal and enter -
    npm i --save-dev browser-sync gulp gulp-autoprefixer gulp-clean gulp-file-include gulp-imagemin gulp-js-minify gulp-rename gulp-sass reset-css ; 


## Test command
 gulp dev 
 - Starting the server and then tracking changes to the `* .js` and` * .scss` files in the `src` folder;
 
 gulp build 
 - clearing the `dist` folder;
   - compilation of `scss` files in` css`;
   - adding vendor prefixes to CSS properties to support the last few versions of each browser;
   - deletion of unused css-code;
   - concatenation of all `css` files into one` styles.min.css` and its minification;
   - concatenation of all `js` files into one` scripts.min.js` and its minification;
   - copying the minified final `styles.min.css` and` scripts.min.js` files to the `dist` folder;
   - optimizing images and copying them to the folder `dist / img`;

gulp cleanFolder - clean folder dist;

## Used technologies
HTML5
CSS3
SASS
JavaScript
Gulp
БЭМ
Git

## The Group
only _ Dima Sobchenko - Od1Seu(slark);

## Task 
All tasks was done  - Od1Seu;




